<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Webentwicklung - PHP</title>

  <!-- Bootstrap core CSS -->
  <!--<link href="css/bootstrap-grid.min.css" rel="stylesheet">-->
  <link href="css/bootstrap.css" rel="stylesheet">

</head>

<body>

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark static-top">
    <div class="container">
      <a class="navbar-brand" href="#">Start</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link" href="#">Home
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Contact</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  

  <!-- Page Content -->

  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
        <form action="" method="post">
          <label>Additional Comments:</label><br>
          <textarea cols="35" rows="12" name="comments" id="para1"></textarea><br>
          <input type="submit" name="button" value="Submit"/></form>
          <br><br>
          <?php
            error_reporting(0);
            $comments= $_POST['comments'];
          ?>  
          <h1><?php
            echo "The comment that you entered is:" . "<br><br>" . $comments;
          ?></h1>
      </div>
    </div>
  </div>
  


 
 
 
  <!-- Bootstrap core JavaScript -->
  <script src="jquery/jquery.slim.min.js>"></script>
  <script src="js/bootstrap.bundle.min.js"></script>

</body>

</html>
